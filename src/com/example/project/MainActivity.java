package com.example.project;


import java.util.Random;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener{
	
	String[] quizAll = new String[] {
			"BLACK","ADOPT", "RIFLE", "STUDY", "PHOTO", "IMAGE", "SERVE", "HOUSE", "QUITE", "CLEAR", "ALIVE", "STORY",
			"FIGHT", "UNTIL", "LEAVE", "POWER", "WRITE", "ABOUT", "NIGHT",  "STARE", "COUNT", "REBEL", "PROUD", "COVER",
			"QUIET", "THREE", "DEATH", "HONOR", "NEVER", "STATE", "AGAIN", "WHEEL", "DRIVE", "ALONG"
	};
	
	int totalQuiz = quizAll.length;
    int usedQuiz = 10;
    int totalChar = 5;
    int totalHiddenChar = 2;
    int maxAns = 5;
    
    Integer quizNumber;
    Integer totalPoint;
    Integer point;
    int totalAns;
    int trueAns;
    Integer[] hiddenChar;
    String[] correctChar;
    String[] quiz;
	
	//private void loadPage(){
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		((Button) findViewById(R.id.buttonA)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonB)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonC)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonD)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonE)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonF)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonG)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonH)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonI)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonJ)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonK)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonL)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonM)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonN)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonO)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonP)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonQ)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonR)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonS)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonT)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonU)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonV)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonW)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonX)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonY)).setOnClickListener(this);
		((Button) findViewById(R.id.buttonZ)).setOnClickListener(this);
		creQuizArray();
	}
	
	private void creQuizArray(){
		//System.out.println("Enter creQuizArray");
        //create array quiz
        Boolean[] chkQuiz = new Boolean[totalQuiz];
        quiz = new String[usedQuiz];
        hiddenChar = new Integer[totalHiddenChar];
        int ranQuiz;
        int i = 0;
        quizNumber = 0;
        totalPoint = 0;
        while (i!=usedQuiz) {
            ranQuiz = new Random().nextInt(totalQuiz);
            if(chkQuiz[ranQuiz]==null){
                chkQuiz[ranQuiz]=true;
                quiz[i] = quizAll[ranQuiz];
                i++;
            }
        }
        ((TextView) findViewById(R.id.labelNum)).setText("1");
        ((TextView) findViewById(R.id.labelTotalPoint)).setText("0");
        //System.out.println("Exit creQuizArray");
        nextQuiz();
    }
    
    private void buildQuiz(){
    	System.out.println("Enter buildQuiz");
        //get hidden index
        Boolean[] chkChar = new Boolean[totalChar];
        int ranChar;
        int i = 0;
        while (i!=totalHiddenChar) {            
            ranChar = new Random().nextInt(totalChar);
            if(chkChar[ranChar]==null){
                chkChar[ranChar]=true;
                hiddenChar[i] = ranChar;
                i++;
            }
            //if(i==totalHiddenChar) break;
        }

        correctChar = new String[totalHiddenChar];
        for (Integer charIndex : hiddenChar) {
            switch(charIndex){
                case 0: ((TextView) findViewById(R.id.pos1)).setText("_"); break;
                case 1: ((TextView) findViewById(R.id.pos2)).setText("_"); break;
                case 2: ((TextView) findViewById(R.id.pos3)).setText("_"); break;
                case 3: ((TextView) findViewById(R.id.pos4)).setText("_"); break;
                case 4: ((TextView) findViewById(R.id.pos5)).setText("_"); break;
            }
        }
        
        System.out.println("quiz is "+quiz[quizNumber]);
        for (int j = 0; j < quiz[quizNumber].length(); j++) {
            if(j != hiddenChar[0] && j != hiddenChar[1]){
                switch(j){
                    case 0: ((TextView) findViewById(R.id.pos1)).setText(quiz[quizNumber].substring(j, j+1)); break;
                    case 1: ((TextView) findViewById(R.id.pos2)).setText(quiz[quizNumber].substring(j, j+1)); break;
                    case 2: ((TextView) findViewById(R.id.pos3)).setText(quiz[quizNumber].substring(j, j+1)); break;
                    case 3: ((TextView) findViewById(R.id.pos4)).setText(quiz[quizNumber].substring(j, j+1)); break;
                    case 4: ((TextView) findViewById(R.id.pos5)).setText(quiz[quizNumber].substring(j, j+1)); break;
                }
            }else if(j == hiddenChar[0]){
                correctChar[0] =  quiz[quizNumber].substring(j, j+1);
            }else if(j == hiddenChar[1]){
                correctChar[1] =  quiz[quizNumber].substring(j, j+1);
            }
        }
        quizNumber++;
        System.out.println("correctChar is "+correctChar[0]+" and "+correctChar[1]);
        System.out.println("Exit buildQuiz");
    }
    
    private Boolean doQuiz(String ans){
    	System.out.println("Click "+ans);
        for (int i = 0; i < correctChar.length; i++) {
            if(correctChar[i]!=null){
            	System.out.println("correctChar["+i+"]!=null");
                if(correctChar[i].equalsIgnoreCase(ans)){
                	System.out.println("correctChar["+i+"].equalsIgnoreCase("+ans+")");
                    switch(hiddenChar[i]){
                        case 0: ((TextView) findViewById(R.id.pos1)).setText(correctChar[i]); break;
                        case 1: ((TextView) findViewById(R.id.pos2)).setText(correctChar[i]); break;
                        case 2: ((TextView) findViewById(R.id.pos3)).setText(correctChar[i]); break;
                        case 3: ((TextView) findViewById(R.id.pos4)).setText(correctChar[i]); break;
                        case 4: ((TextView) findViewById(R.id.pos5)).setText(correctChar[i]); break;
                    }
                    correctChar[i] = null;
                    return true;
                }
            }
        }
        System.out.println("doQuiz???");
        return false;
    }
	
	private void pointChk(boolean chk){
		totalAns += 1;
        if(chk){
        	System.out.println("Correct Answer");
            trueAns += 1;
            if(totalAns==maxAns || trueAns==2){
                totalPoint += point;
                if(quizNumber!=quiz.length){
                	System.out.println("End this quiz and quizNumber = "+quizNumber+": quiz.length = "+quiz.length);
                    nextQuiz();
                }else{
                    System.out.println("Finish quiz");
                    creQuizArray();
                }
            }
        }else{
        	System.out.println("False Answer");
            point -= 2;
            ((TextView) findViewById(R.id.labelPoint)).setText(point.toString());
            if(totalAns==maxAns){
            	System.out.println("End this quiz and totalAns = "+totalAns);
            	totalPoint += point;
            	if(quizNumber!=quiz.length){
                	System.out.println("End this quiz and quizNumber = "+quizNumber+": quiz.length = "+quiz.length);
                    nextQuiz();
                }else{
                    System.out.println("Finish quiz");
                    creQuizArray();
                }
            }
        }
    }
    
    private void nextQuiz(){
    	System.out.println("Enter nextQuiz");
    	Integer num = quizNumber+1;
    	((TextView) findViewById(R.id.labelNum)).setText(num.toString());
    	((TextView) findViewById(R.id.labelPoint)).setText("10");
    	((TextView) findViewById(R.id.labelTotalPoint)).setText(totalPoint.toString());
        buildQuiz();
        point = 10;
        totalAns = 0;
        trueAns = 0;
        System.out.println("Exit nextQuiz");
    }
	
	
	

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		/*@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}*/
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		switch(id){
			case R.id.buttonA:pointChk(doQuiz("A")); break;
			case R.id.buttonB:pointChk(doQuiz("B")); break;
			case R.id.buttonC:pointChk(doQuiz("C")); break;
			case R.id.buttonD:pointChk(doQuiz("D")); break;
			case R.id.buttonE:pointChk(doQuiz("E")); break;
			case R.id.buttonF:pointChk(doQuiz("F")); break;
			case R.id.buttonG:pointChk(doQuiz("G")); break;
			case R.id.buttonH:pointChk(doQuiz("H")); break;
			case R.id.buttonI:pointChk(doQuiz("I")); break;
			case R.id.buttonJ:pointChk(doQuiz("J")); break;
			case R.id.buttonK:pointChk(doQuiz("K")); break;
			case R.id.buttonL:pointChk(doQuiz("L")); break;
			case R.id.buttonM:pointChk(doQuiz("M")); break;
			case R.id.buttonN:pointChk(doQuiz("N")); break;
			case R.id.buttonO:pointChk(doQuiz("O")); break;
			case R.id.buttonP:pointChk(doQuiz("P")); break;
			case R.id.buttonQ:pointChk(doQuiz("Q")); break;
			case R.id.buttonR:pointChk(doQuiz("R")); break;
			case R.id.buttonS:pointChk(doQuiz("S")); break;
			case R.id.buttonT:pointChk(doQuiz("T")); break;
			case R.id.buttonU:pointChk(doQuiz("U")); break;
			case R.id.buttonV:pointChk(doQuiz("V")); break;
			case R.id.buttonW:pointChk(doQuiz("W")); break;
			case R.id.buttonX:pointChk(doQuiz("X")); break;
			case R.id.buttonY:pointChk(doQuiz("Y")); break;
			case R.id.buttonZ:pointChk(doQuiz("Z")); break;
				
		}
	}

}
